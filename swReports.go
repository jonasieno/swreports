// Package swReports implements the package that calculate how many stops will be required to travel a given distance, with each spacecraft listed at https://swapi.co.
package swreports

import (
    "fmt"
    "strings"
    "strconv"
    // Package swapi is a 3rd party package that implemets swapi´s access.
    "github.com/peterhellberg/swapi"
)

const dayHrs   int = 24
const weekHrs  int = dayHrs * 7
const monthHrs int = dayHrs * 30
const yearHrs  int = dayHrs * 365

// HrsMap is a const map that converts default strings terms in hours.
var hrsMap = map[string]int {
    "day"   : dayHrs  ,
    "days"  : dayHrs  ,
    "week"  : weekHrs ,
    "weeks" : weekHrs ,
    "month" : monthHrs,
    "months": monthHrs,
    "year"  : yearHrs ,
    "years" : yearHrs ,
}

// StarshipStopsNeededReport struct is the type returned as result of the StarshipsStopsNeededByDistanceReport function call.
type StarshipStopsNeeded struct {
    Name        string
    StopsNeeded int
}

// StarshipsStopsNeededByDistanceReport function
func StarshipsStopsNeededByDistanceReport(discance_MGLT int, debug bool) []StarshipStopsNeeded {
    var result []StarshipStopsNeeded

    if debug {
        fmt.Println("[in ] StarshipsStopsNeededByDistanceReport(", discance_MGLT, ")")
    }

    swapiClient := swapi.DefaultClient

    if starships, err := swapiClient.Starships(); err == nil {

        for _, starship := range starships {

            var thisResult  StarshipStopsNeeded
            var stopsNeeded int

            if debug {
                fmt.Println("")
                fmt.Println("DADOS RECEBIDOS:")
                fmt.Println("  Name       :", starship.Name       )
                fmt.Println("  Consumables:", starship.Consumables)
                fmt.Println("  MGLT       :", starship.MGLT       )
                fmt.Println("  Processando...")
            }
            // Converting string to hours.
            consumablesSplited := strings.Split(starship.Consumables, " ")
            
            if consumablesValue, err := strconv.Atoi(consumablesSplited[0]); err == nil {
                consumablesUnity_hr := hrsMap[consumablesSplited[1]]

                var consumables_hr int = (consumablesValue * consumablesUnity_hr)

                if mgltPerHour, err := strconv.Atoi(starship.MGLT);  err == nil {

                    // StopsNeeded variable is calculated throught: discance_MGLT / (starship.MGLT * starship.Consumables) in hour.
                    stopsNeeded = discance_MGLT / (mgltPerHour * consumables_hr)

                    if debug {
                        fmt.Println("  consumablesSplited[1]:", consumablesSplited[1])
                        fmt.Println("  consumablesValue     :", consumablesValue     )
                        fmt.Println("  consumablesUnity_hr  :", consumablesUnity_hr  )
                        fmt.Println("  consumables_hr       :", consumables_hr       )
                        fmt.Println("  mgltPerHour          :", mgltPerHour          )
                        fmt.Println("  StopsNeeded          :", stopsNeeded          )
                    }
                }
            }

            thisResult = StarshipStopsNeeded{starship.Name, stopsNeeded}

            result = append(result, thisResult)
        }
    }

    if debug {
        fmt.Println("[out] StarshipsStopsNeededByDistanceReport(", discance_MGLT, "): ", result)
    }

    return result
}
