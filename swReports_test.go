package swreports

import (
    "fmt"
    "testing"
)

func TestStarshipsStopsNeededByDistanceReport(t *testing.T) {

    distance_MGLT := 1000000
    found         := 0

    // Wants array is the acceptance rules to the test.
    wants := []StarshipStopsNeeded {
            {"Millennium Falcon",  9},
            {"Y-wing"           , 74},
            {"Rebel transport"  , 11},
        }

    fmt.Println("---                G E T R A K                 ---")
    fmt.Println("--- Unit Test for the Star Wars Report Package ---")
    fmt.Println("---     Wrote by jonasieno@gmail.com V1R00     ---")
    fmt.Println("\r\n[+] Starting...")

    // Starships array has the result of the star wars report request.
    starships := StarshipsStopsNeededByDistanceReport(distance_MGLT)

    fmt.Println("[+] len(starships): ", len(starships))

    // For loop apply the test with the report result.
    for _, received := range starships {
        fmt.Println("[~] Received: ", received)

        for _, want := range wants {
            if want == received {
                fmt.Println("[+] Found   : ", want)
                found = found + 1
            }
        }
    }

    if found != len(wants) {
        t.Errorf("StarshipsStopsNeededByDistanceReport(%v) fail! Tested %v, found %v but wanted %v.", distance_MGLT, len(starships), found, len(wants))
    }

}